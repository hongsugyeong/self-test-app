import React, { useState } from 'react';

const Ddong = () => {
    const [currentQuestion, setCurrentQuestion] = useState(0)
    // 관리할 값: currentQuestion, 변경시킬 함수: setCurrentQuestion
    const [score, setScore] = useState(0)
    // 관리할 값: score, 변경시킬 함수: setScore

    const questions = [
        {
            question: '변의 색깔은 어떠셨나요?',
            answers: [
                {text: "황갈색", score: 10},
                {text: "붉은색", score: 0},
                {text: "검정색", score: 0},
                {text: "회백색", score: 0},
                {text: "초록색", score: 5},
                {text: "노란색", score: 5}
            ],
        },
        {
            question: '변의 형태는 어떠셨나요?',
            answers: [
                {text: "바나나 모양", score: 10},
                {text: "가늘고 긴 모양", score: 5},
                {text: "묽은 상태", score: 5},
                {text: "자잘한 모양", score: 5},
                {text: "끊긴 모양", score: 5}
            ],
        },
        {
            question: '변을 보는 시간은 어떻게 되나요?',
            answers: [
                {text: "5분 이내", score: 10},
                {text: "10분 이내", score: 5},
                {text: "15분 이내", score: 0}
            ],
        },
        {
            question: '변을 보는 빈도는 어떻게 되나요?',
            answers: [
                {text: "하루에 1 ~ 3번", score: 10},
                {text: "2 ~ 3일에 한 번", score: 5},
                {text: "4일에 한 번", score: 5},
                {text: "일주일에 한 번", score: 0},
                {text: "하루에 4번 이상", score: 0}
            ],
        },
        {
            question: '변을 볼 때 통증이나 불편함을 느끼시나요?',
            answers: [
                {text: "없음", score: 10},
                {text: "간혹 느낌", score: 5},
                {text: "항상 느낌", score: 0}
            ],
        },
        {
            question: '일주일 평균 운동량은 어떻게 되시나요?',
            answers: [
                {text: "4회 이상", score: 10},
                {text: "1 ~ 4회", score: 5},
                {text: "0회", score: 5}
            ],
        },
        {
            question: '특정 약물을 복용하고 계신가요?',
            answers: [
                {text: "먹고 있지 않다.", score: 10},
                {text: "먹고 있다.", score: 0}
            ],
        },
    ]

    const getResult = score => {
        if (score >= 60) {
            return "당신은 최고의 응가를 싸고 있군요~!"
        } else {
            return "당신은 최악의 응가를 싸고 있군요~!"
        }
    }

    const handleAnswerClick = answerScore => {
        setScore(score + answerScore)
        // 스코어 + 답변스코어 => 쌓이는 거?
        const nextQuestion = currentQuestion + 1
        // 다음 질문 선언해주기
        if (nextQuestion < questions.length) {
            // 다음 질문으로 넘어가는 조건 세우기
            setCurrentQuestion(nextQuestion)
            // 다음 질문 준비하기
        } else {
            alert(getResult(score + answerScore))
            // 마지막 질문일 경우 점수 계산하기
            // 이 코드에서는 alert창을 통해 결과를 보여줌
        }
    }

    return (
        <div>
            <h2>{questions[currentQuestion].question}</h2>
            {/* 현재 질문 넣기 */}
            {questions[currentQuestion].answers.map((answer) => (
                // 보기 넣기 => 위에서 쓴 questions 속 answer 보여질 수 있도록 map(= 순환)을 통해 버튼 보여줌
                <button key={answer.text} onClick={() => handleAnswerClick(answer.score)}>
                    {/* key로 answer.text를 넘겨줌
                    앞에서 선언한 handleAnswerClick를 통해 점수를 계산하고 다음 질문으로 가기 answer.score가 answerScore로 넘어가는 거? */}
                    {answer.text}
                {/*  이건 단순히 보기에 글자 넣은 거임  */}
                </button>
            ))}
        </div>
    )
}

export default Ddong;